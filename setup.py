from setuptools import setup, find_packages

classifiers = [
    'Development status::',
    'Intended Audience::',
    'License:: OSI Approved :: MIT License',
    'Programming Language :: Python :: 3'
]

setup(
    name="software name",
    version="software version",
    description="",
    Long_description="",
    url="",
    author="",
    author_email="",
    License="",
    classifiers="",
    keyword="",
    packages="",
    install_requires=[""]
)